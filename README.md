# monstub -- mini GDB-stub to read memory of a child process

The `monstub` utility allows to read arbitrary memory of child processes
in Linux for monitoring purposes.

As interface towards a monitor application,
`monstub` provides a TCP port with the
[GDB remote protocol](https://sourceware.org/gdb/onlinedocs/gdb/Remote-Protocol.html).
Debuggers like GDB can then connect and access memory of a child process.

`monstub` uses the `process_vm_readv()` system call to read memory
inside a child process. The child process and its parameters
are provided on the command line.

## Compilation

Compile `monstub` and a simple test application named `test`:
```
$ gcc -Og -o monstub monstub.c
$ gcc -Og -o test test.c
```

## Usage

On the command line, invoke `monstub`
with port number for the GDB-server, e.g. `1234`,
and the child process to start:
```
$ ./monstub <gdb-port> <program> [<args>]
```

## Example

We use the `test` application as dummy payload to show how `monstub` works.

The test application prints the addresses and the content of some variables,
and then waits for termination:
```
short int g = 42;
const char *s = "Hello, World";

int main(int argc, char *argv[])
{
    int l = getpid();

    printf("# g: %p,%zx = %d 0x%x\n", &g, sizeof(g), g, g);
    printf("# l: %p,%zx = %d 0x%x\n", &l, sizeof(l), l, l);
    printf("# s: %p,%zx = %p -> \"%s\"\n", &s, sizeof(s), s, s);

    pause();

    return 0;
}
```

We start `monstub` with the GDB-server on port `1234`
and the test application as child process:
```
$ ./monstub 1234 ./test
# g: 0x55d1cd6cb010,2 = 42 0x2a
# l: 0x7fff9b1e4404,4 = 20059 0x4e5b
# s: 0x55d1cd6cb018,8 = 0x55d1cd6c904c -> "Hello, World"
```
The test prints its output to the console
and then waits for termination.

In a second terminal, we start GDB to inspect the process:
```
$ gdb
GNU gdb (Ubuntu 9.2-0ubuntu1~20.04.1) 9.2
Copyright (C) 2020 Free Software Foundation, Inc.
[...]
For help, type "help".
Type "apropos word" to search for commands related to "word".
(gdb)
```

In GDB, we first set the architecture to 64-bit x86,
as my test happens on a 64-bit Linux host:
```
(gdb) set architecture i386:x86-64
The target architecture is assumed to be i386:x86-64
(gdb)
```
and then connect to the GDB stub of `monstub` on port 1234:
```
(gdb) target remote :1234
Remote debugging using :1234
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x0000000000000000 in ?? ()
(gdb)
```
Note that `monstub` never halts the process, nor has it any understanding
of threads inside the process, so it always returns address zero
as instruction pointer. GDB ignores this gracefully.

We can now access the memory of the child process
and, for example, print the content of the test variables:
```
(gdb) p *(int*)0x55d1cd6cb010
$1 = 42
(gdb) p (char*)0x55d1cd6c904c
$2 = 0x55d1cd6c904c "Hello, World"
(gdb)
```

We finally quit GDB:
```
(gdb) q
A debugging session is active.

	Inferior 1 [Remote target] will be detached.

Quit anyway? (y or n) y
Detaching from program: , Remote target
Ending remote debugging.
[Inferior 1 (Remote target) detached]
$
```

The GDB-stub provides just the minimal commands to query registers
(always return 0) and to provide read access to the memory of the child process.
The remote process appears to be stopped all the time.
Commands to single-step or continue the process are implemented as NOPs.

Also, the `process_vm_readv()` system call requires special privileges
to read the memory of arbitrary processes. We implicitly have these privileges
to debug child processes (the same limitation as for `ptrace()`),
so we do not require execution as root user.
There is no support for writing memory, but this could be implemented.
