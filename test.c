/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * test.c
 *
 * Simple test application for GDB-stub
 *
 * Compile:
 *   $ gcc -Og -o test test.c
 *
 * Usage:
 *   $ ./test
 *
 * azuepke, 2022-04-21: initial
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

short int g = 42;
const char *s = "Hello, World";

int main(int argc, char *argv[])
{
	int l = getpid();

	printf("# g: %p,%zx = %d 0x%x\n", &g, sizeof(g), g, g);
	printf("# l: %p,%zx = %d 0x%x\n", &l, sizeof(l), l, l);
	printf("# s: %p,%zx = %p -> \"%s\"\n", &s, sizeof(s), s, s);

	pause();

	return 0;
}
