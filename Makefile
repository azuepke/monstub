# SPDX-License-Identifier: MIT
# Copyright 2022 Alexander Zuepke
#
# Makefile
#
# azuepke, 2022-04-21: initial

CFLAGS = -W -Og

.PHONY: all clean distclean

all: monstub test

monstub: monstub.c
	$(CC) $(CFLAGS) -o $@ $^

test: test.c
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -f monstub test

distclean: clean
