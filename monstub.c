/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * monstub.c
 *
 * Mini GDB-stub to read memory in a child process
 *
 * Compile:
 *   $ gcc -Og -o monstub monstub.c
 *
 * Usage:
 *   $ ./monstub <gdb-port> <program> [<args>]
 *
 * See https://sourceware.org/gdb/onlinedocs/gdb/Remote-Protocol.html
 * for a description of the GDB remote protocol.
 *
 * azuepke, 2022-04-21: initial
 */

#define _GNU_SOURCE
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <linux/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


/* error and out */
#define perror_exit(msg)	\
	do { perror(msg); exit(EXIT_FAILURE); } while (0)


/* checksum a string (sum of bytes modulo 256) */
static unsigned int csum_string(const char *s, size_t sz)
{
	unsigned int csum;
	size_t pos;

	csum = 0;
	for (pos = 0; pos < sz; pos++) {
		csum += s[pos];
	}
	csum &= 0xff;

	return csum;
}

static const char hex[16] = "0123456789abcdef";

static void reply_nak(int fd_a)
{
	ssize_t s;

	s = write(fd_a, "-", 1);
	if (s != 1) {
		perror_exit("write");
	}
}

/* reply raw data encapsulated in a packet */
static void reply_binary(int fd_a, const char *raw_data, size_t sz)
{
	char buf[2 + sz * 2 + 3];
	unsigned int csum;
	size_t pos;
	char c;
	ssize_t s;

	pos = 0;
	buf[pos++] = '+';
	buf[pos++] = '$';

	for (size_t i = 0; i < sz; i++) {
		c = raw_data[i];
		buf[pos++] = hex[(c >> 4) & 0xf];
		buf[pos++] = hex[(c >> 0) & 0xf];
	}

	csum = csum_string(&buf[2], pos - 2);
	buf[pos++] = '#';
	buf[pos++] = hex[(csum >> 4) & 0xf];
	buf[pos++] = hex[(csum >> 0) & 0xf];

	s = write(fd_a, buf, pos);
	if (s != (ssize_t)pos) {
		perror_exit("write");
	}
}

/* reply string data encapsulated in a packet */
static void reply_string(int fd_a, const char *str_data)
{
	char buf[1024];
	unsigned int csum;
	size_t pos;
	ssize_t s;

	pos = 0;
	buf[pos++] = '+';
	buf[pos++] = '$';

	for (size_t i = 0; str_data[i] != '\0'; i++) {
		buf[pos++] = str_data[i];
	}

	csum = csum_string(&buf[2], pos - 2);
	buf[pos++] = '#';
	buf[pos++] = hex[(csum >> 4) & 0xf];
	buf[pos++] = hex[(csum >> 0) & 0xf];

	s = write(fd_a, buf, pos);
	if (s != (ssize_t)pos) {
		perror_exit("write");
	}
}


/* parse an incoming packet */
static void parse_packet(int fd_a, pid_t pid, const char *cbuf)
{
	unsigned long long addr;
	unsigned long size;
	char *end;

	if (memcmp(cbuf, "qSupported", 10) == 0) {
		/* initial packet by GDB */
		reply_string(fd_a, "PacketSize=3fc");
		return;
	}

	if (memcmp(cbuf, "vCont?", 6) == 0) {
		/* query vCont support -- not supported */
		reply_string(fd_a, "");
		return;
	}

	if (memcmp(cbuf, "?", 1) == 0) {
		/* query if target is halted */
		reply_string(fd_a, "S00");
		return;
	}

	if (memcmp(cbuf, "D", 1) == 0) {
		/* detach */
		reply_string(fd_a, "OK");
		return;
	}

	if ((memcmp(cbuf, "c", 1) == 0) ||
	    (memcmp(cbuf, "C", 1) == 0) ||
	    (memcmp(cbuf, "s", 1) == 0) ||
	    (memcmp(cbuf, "S", 1) == 0) ||
	    (memcmp(cbuf, "vCont", 5) == 0)) {
		/* continue or step, normally no reply, but we reply with "stopped" */
		reply_string(fd_a, "S00");
		return;
	}

	if (memcmp(cbuf, "qC", 2) == 0) {
		/* query if target is halted */
		reply_string(fd_a, "QC0");
		return;
	}

	if (memcmp(cbuf, "g", 1) == 0) {
		/* query all registers (truncated) */
		reply_string(fd_a, "0000000000000000");
		return;
	}

	if (memcmp(cbuf, "p", 1) == 0) {
		/* query specific registers -- we return 0 for all registers */
		reply_string(fd_a, "0000000000000000");
		return;
	}

	if (memcmp(cbuf, "qAttached", 9) == 0) {
		/* query if GDB is attached */
		reply_string(fd_a, "1");
		return;
	}

	if (memcmp(cbuf, "m", 1) == 0) {
		/* read memory: addr, size */
		addr = strtoull(cbuf+1, &end, 16);
		if (*end != ',') {
			goto return_einval;
		}
		size = strtoul(end+1, &end, 16);
		if (*end != '\0') {
			goto return_einval;
		}
		if (size == 0 || size > 512) {
			goto return_efault;
		}

		{
			struct iovec local_iov;
			struct iovec remote_iov;
			char mbuf[512];
			ssize_t rs;

			local_iov.iov_base = mbuf;
			local_iov.iov_len = size;
			remote_iov.iov_base = (void *)addr;
			remote_iov.iov_len = size;

			rs = process_vm_readv(pid, &local_iov, 1, &remote_iov, 1, 0);
			if (rs != (ssize_t)size) {
				goto return_efault;
			}

			reply_binary(fd_a, mbuf, size);
		}
		return;
	}

	/* unsupported command -- send empty string as answer */
	reply_string(fd_a, "");
	return;

return_einval:
	/* return error code for EINVAL */
	reply_string(fd_a, "E22");
	return;

return_efault:
	/* return error code for EFAULT */
	reply_string(fd_a, "E14");
	return;
}

/* checksum an incoming packet */
static void checksum_packet(int fd_a, pid_t pid, const char *cbuf, size_t cbuf_sz, char csum1, char csum2)
{
	char csum_as_string[3] = { csum1, csum2, 0 };
	unsigned int csuma;
	unsigned long csumb;
	char *end;

	csuma = csum_string(cbuf, cbuf_sz);
	csumb = strtoul(csum_as_string, &end, 16);

	if ((end != &csum_as_string[2]) || csuma != csumb || cbuf_sz == 0) {
		/* invalid hex in checksum or checksum error or zero size */
		reply_nak(fd_a);
		return;
	}

	parse_packet(fd_a, pid, cbuf);
}


int main(int argc, char *argv[], char *envp[])
{
	struct sockaddr_in addr;
	socklen_t addrlen;
	char rbuf[1024];
	char cbuf[1024];
	ssize_t rbuf_pos;
	size_t cbuf_pos;
	enum {
		P_UNKNOWN,
		P_DATA,
		P_CSUM1,
		P_CSUM2,
	} parser_state;
	char csum1 = 0;
	char csum2 = 0;
	char c;
	pid_t pid;
	int port;
	int flag;
	int fd_b;
	int fd_a;
	int rc;
	ssize_t sz;

	if (argc < 3) {
		fprintf(stderr, "usage: monstub <gdb-port> <program> [<args>]\n");
		exit(EXIT_FAILURE);
	}
	port = atoi(argv[1]);

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port);

	fd_b = socket(AF_INET, SOCK_STREAM, 0);
	if (fd_b == -1) {
		perror_exit("socket");
	}

	flag = 1;
	rc = setsockopt(fd_b, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
	if (rc == -1) {
		perror_exit("setsockopt SO_REUSEADDR");
	}

	flag = 1;
	rc = setsockopt(fd_b, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(flag));
	if (rc == -1) {
		perror_exit("setsockopt TCP_NODELAY");
	}

	rc = bind(fd_b, (struct sockaddr *) &addr, sizeof(addr));
	if (rc == -1) {
		perror_exit("bind");
	}

	rc = listen(fd_b, 1);
	if (rc == -1) {
		perror_exit("listen");
	}

	pid = fork();
	if (pid == -1) {
		perror_exit("fork");
	} else if (pid == 0) {
		/* child process */
		close(fd_b);
		execve(argv[2], argv+2, envp);
		perror_exit("execve");
	}

	/* parent process */
	while (1) {
		addrlen = sizeof(addr);
		fd_a = accept(fd_b, (struct sockaddr *) &addr, &addrlen);
		if (fd_a == -1) {
			perror_exit("accept");
		}

		cbuf_pos = 0;
		parser_state = P_UNKNOWN;
		do {
			sz = read(fd_a, rbuf, sizeof(rbuf));
			if (sz == 0) {
				/* EOF from socket */
				break;
			}
			if (sz == -1) {
				/* error */
				perror_exit("read from socket");
			}

			for (rbuf_pos = 0; rbuf_pos < sz; rbuf_pos++) {
				c = rbuf[rbuf_pos];
				if (1 /* any state */ && c == '\003') {
					/* break -- drop previous packet */
					cbuf_pos = 0;
					parser_state = P_UNKNOWN;
					continue;
				}
				if (1 /* any state */ && c == '$') {
					/* start of new packet -- drop previous packet */
					cbuf_pos = 0;
					parser_state = P_DATA;
					continue;
				}
				if (parser_state == P_DATA && c == '#') {
					parser_state = P_CSUM1;
					continue;
				}
				if (parser_state == P_CSUM1) {
					csum1 = c;
					parser_state = P_CSUM2;
					continue;
				}
				if (parser_state == P_CSUM2) {
					csum2 = c;
					/* clear out rest of buffer, this simplifies the parser */
					memset(&cbuf[cbuf_pos], 0, sizeof(cbuf) - cbuf_pos);
					checksum_packet(fd_a, pid, cbuf, cbuf_pos, csum1, csum2);
					parser_state = P_UNKNOWN;
					continue;
				}
				if (cbuf_pos >= sizeof(cbuf)) {
					/* parse buffer overflow -- drop previous packet */
					cbuf_pos = 0;
					parser_state = P_UNKNOWN;
					continue;
				}
				cbuf[cbuf_pos] = c;
				cbuf_pos++;
			}
		} while (1);

		close(fd_a);
	}

	close(fd_b);
	return EXIT_SUCCESS;
}
